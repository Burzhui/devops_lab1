#!/usr/bin/env bash

apt update && apt install -y curl python3

curl -sSL https://install.python-poetry.org | python3 -

export PATH="$HOME/.local/bin:$PATH"

poetry env use 3.11

poetry install