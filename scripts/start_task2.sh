#!/usr/bin/env bash

docker kill task2-server
docker rm task2-server

docker kill task2-client
docker rm task2-client

docker network create task2-network

docker run -d --name task2-server --network task2-network --hostname task2-server -e HOST=0.0.0.0 -p 8000:8000 task2:latest python -m server
sleep 2
docker run -d --name task2-client --network task2-network --hostname task2-client -e SERVER_URL=http://task2-server:8000 -p 5000:5000 task2:latest python -m client