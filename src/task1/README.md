# Task1

This simple Python script generates random email addresses with a specified domain.

## Usage

```bash
python -m src.task1
```

## Interface

```text
usage: __main__.py [-h] [--domain DOMAIN] [-n N]

Generate a random email

options:
  -h, --help       show this help message and exit
  --domain DOMAIN  Domain name
  -n N             Number of generated email addreses

```