## Interface

```text
usage: __main__.py [-h] [--domain DOMAIN] [-n N]

Generate a random email

options:
  -h, --help       show this help message and exit
  --domain DOMAIN  Domain name
  -n N             Number of generated email addreses

```