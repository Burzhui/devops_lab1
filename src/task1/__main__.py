import argparse
import random

import uuid


def generate_random_email_uuid(domain: str):
    length = random.randint(10, 14)
    username = str(uuid.uuid4()).replace("-", "")[:length]
    email_address = f"{username}@{domain}"
    return email_address


def main():
    parser = argparse.ArgumentParser(description="Generate a random email")
    parser.add_argument(
        "--domain",
        type=str,
        default="gmail.com",
        required=False,
        help="Domain name",
    )
    parser.add_argument(
        "-n",
        type=int,
        default=1,
        required=False,
        help="Number of generated email addreses",
    )
    args = parser.parse_args()
    try:
        generated_email_addreses = [
            generate_random_email_uuid(args.domain) for i in range(args.n)
        ]
        plain_text = "\n".join(generated_email_addreses)
        print(plain_text)
    except ValueError as e:
        print(e)
        raise SystemExit


if __name__ == "__main__":
    main()
