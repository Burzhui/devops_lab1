## [server](server)

Pprovides an endpoint `/students` to
retrieve student names and birthdates from a SQLite database. The server initializes the
database [schema.sql](server%2Fschema.sql) and generates
fake student data on startup.

## [client](client)

The `__main__.py` script in the `client` directory is a Python client that periodically fetches student data from a
server using HTTP requests. It retrieves information from the `/students` endpoint, pretty-prints the data, and saves it
to a local file named `output.json`. The script runs in an infinite loop with a 5-second delay between requests.