import os
import pathlib
import random
import sqlite3
import uuid
from contextlib import contextmanager
from os import getenv
from sqlite3 import Cursor, Row

import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI
from mimesis import Generic, Datetime, Person

load_dotenv()

app = FastAPI()

SERVER_HOST = os.getenv("SERVER_HOST", "localhost")
SERVER_PORT = os.getenv("SERVER_HOST", 8001)
DATABASE_PATH = pathlib.Path(__file__).parent.joinpath("university.db")


@contextmanager
def sqlite_cursor() -> Cursor:
    connection = sqlite3.connect(DATABASE_PATH)
    cursor = connection.cursor()
    cursor.row_factory = Row
    try:
        yield cursor
        connection.commit()
    except Exception as e:
        connection.rollback()
        raise e
    finally:
        cursor.close()


def generate_fake_data(
    cursor,
    num_students=60,
    min_subjects=3,
    max_subjects=5,
):
    generic = Generic()
    datetime_provider = Datetime()
    person_provider = Person()

    for _ in range(num_students):
        cursor.execute(
            """
            INSERT INTO students (username, first_name, last_name, birth_date)
            VALUES (?, ?, ?, ?)
        """,
            (
                str(uuid.uuid4()),
                person_provider.first_name(),
                person_provider.last_name(),
                datetime_provider.date(),
            ),
        )

    student_ids = [
        row[0] for row in cursor.execute("SELECT id FROM students").fetchall()
    ]
    for student_id in student_ids:
        try:
            group_name = random.choice([f"БСБО-{x}-20" for x in range(1, 8)])
            cursor.execute(
                """
                INSERT INTO study_group (group_name, student_id)
                VALUES (?, ?)
            """,
                (group_name, student_id),
            )
        except Exception as e:
            print(e)
            pass

        for subject in [
            generic.text.word()
            for x in range(random.randint(min_subjects, max_subjects))
        ]:
            exam_date = datetime_provider.date()
            teacher_full_name = (
                f"{person_provider.first_name()} {person_provider.last_name()}"
            )
            student_id = generic.random.choice(student_ids)
            cursor.execute(
                """
                INSERT INTO gradebook (subject, exam_date, teacher_full_name, student_id)
                VALUES (?, ?, ?, ?)
            """,
                (subject, exam_date, teacher_full_name, student_id),
            )


def on_startup():
    with sqlite_cursor() as cursor:
        cursor.executescript(
            pathlib.Path(__file__)
            .parent.joinpath("schema.sql")
            .read_text(encoding="utf-8")
        )
    with sqlite_cursor() as cursor:
        generate_fake_data(cursor)


app.add_event_handler("startup", on_startup)


@app.get("/students")
async def get_teacher_names():
    with sqlite_cursor() as cursor:
        cursor: Cursor
        students = cursor.execute(
            """
            SELECT first_name  || ' ' || last_name as full_name, birth_date FROM students ORDER BY birth_date
        """
        ).fetchall()
        return [dict(row) for row in students]


if __name__ == "__main__":
    uvicorn.run(app, host=getenv("HOST", "localhost"), port=getenv("PORT", 8000))
