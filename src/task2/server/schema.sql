create table if not exists students (
    id integer primary key autoincrement,
    username text not null,
    first_name text not null,
    last_name text not null,
    birth_date date not null
);

create table if not exists study_group (
    id integer primary key autoincrement,
    student_id integer,
    group_name text not null,
    foreign key (student_id) references students (id)
    unique(student_id, group_name) on conflict replace
);

create table if not exists gradebook (
    id integer primary key autoincrement,
    student_id integer,
    subject text not null,
    exam_date date,
    teacher_full_name text,
    foreign key (student_id) references students (id)
    unique(student_id, subject) on conflict replace
);
