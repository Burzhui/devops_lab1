import json
import os
import pathlib
import time
from pprint import pprint

import requests
from dotenv import load_dotenv

load_dotenv()

SERVER_URL = os.getenv("SERVER_URL", "http://localhost:8000")

if __name__ == "__main__":
    while True:
        with requests.get(f"{SERVER_URL}/students") as resp:
            data = resp.json()
            pprint(data)
            output_path = (
                pathlib.Path(__file__)
                .parent.joinpath("output.json")
                .open("w", encoding="utf-8")
            )
            output_path.write(json.dumps(data, indent=2))
        time.sleep(5)
