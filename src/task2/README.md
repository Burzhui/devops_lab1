# Task2

This Python script allows you to manage and display information about a students

## Usage

local

```bash
python -m src.task2.server
python -m src.task2.client
```

docker

```bash
bash scripts/build_images.sh
bash scripts/start_task2.sh
```

## Artifacts

```bash
docker cp task2-client:/app/client/output.json output.json
```