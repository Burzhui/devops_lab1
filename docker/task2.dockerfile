FROM python:3.11.7-slim-bullseye as build

RUN apt-get update && apt-get install -y curl

RUN pip install poetry==1.4.2

WORKDIR /app

COPY pyproject.toml poetry.lock /app/

RUN poetry config virtualenvs.create false

RUN poetry install -n

COPY src/task2/client ./client
COPY src/task2/server ./server